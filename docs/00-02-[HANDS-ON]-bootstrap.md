# :construction_worker: HANDS-ON: _Bootstrap the workshop_

Wir benutzen ein Linux Live-System mit vorinstallierter Software.

## Rechner von USB-Stick booten
   
  * anschalten
  * F11 drücken
  * im Boot-menü USB-Stick auswählen

## Linux starten

  * ...

## Linux Login
- User: xxx
- Password: yyy
