## Willkommen zum Workshop

# *HANDS-ON CRYPTOCURRENCIES*

mit [Hannes Rohde](h.rohde@tarent.de) und [Daniel Zerlett](d.zerlett@tarent.de).

## Was erwartet euch heute?

### Wir werden
- Bitcoin Wallets erstellen
- Bitcoins empfangen und versenden
- Cryptocurrencies "minen"
- einen Smart Contract deployen und mit ihm interagieren
- ... aber alles auf sogenannten "Testnets" :)

### Abwechselnde Blöcke
- :mortar_board: THEORIE
- :construction_worker: HANDS ON