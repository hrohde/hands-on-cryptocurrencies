# :mortar_board: THEORIE: _Beyond bitcoin_

## Bitcoin ist Urahn aller Cryptocurrencies
- langsam, technisch beschränkt

## Zahlreiche Clones, Forks, Weiterentwicklungen

### schneller / höherer Durchsatz
- Blocktime bei Bitcoin (10 Minuten) sehr konservativ, Konsensfindung viel schneller möglich
- Blockgröße 1MB begrenzt Anzahl der Transaktionen pro Block

### funktionale Erweiterungen
- Privacy (Monero, Zcash)
- Smart Contracts (Ethereum, EOS)

### andere Konzepte
- On-chain governance (Decred)
- Proof-of-X statt Proof-of-Work
- DAG statt Blockchain (IOTA, Byteball)