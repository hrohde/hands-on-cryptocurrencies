# :construction_worker: HANDS-ON: _Mining_

### Pool-Mining für _Monero_ mit _xmrig_:

    cd xmrig/
    ./xmrig

Ausgabe:    
    
    * ABOUT        XMRig/2.14.1 gcc/5.4.0
    * LIBS         libuv/1.24.1 OpenSSL/1.1.1a microhttpd/0.9.62 
    * CPU          Intel(R) Core(TM) i7-4790K CPU @ 4.00GHz (1) x64 AES AVX2
    * CPU L2/L3    1.0 MB/8.0 MB
    * THREADS      4, cryptonight, donate=1%
    * ASSEMBLY     auto:intel
    * POOL #1      monerohash.com:7777 variant auto
    * COMMANDS     hashrate, pause, resume
    [2019-04-08 00:14:51] use pool monerohash.com:7777  107.191.99.221 
    [2019-04-08 00:14:51] new job from monerohash.com:7777 diff 25000 algo cn/r height 1808075
    [2019-04-08 00:14:51] READY (CPU) threads 4(4) huge pages 0/4 0% memory 8192 KB
    [2019-04-08 00:15:54] speed 10s/60s/15m 179.0 178.7 n/a H/s max 183.7 H/s
